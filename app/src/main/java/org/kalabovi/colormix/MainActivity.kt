package org.kalabovi.colormix

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.skydoves.colorpickerview.ColorPickerView
import com.skydoves.colorpickerview.listeners.ColorListener
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.*
import kotlin.math.pow
import kotlin.math.sqrt

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    val picker1 by lazy { findViewById<ColorPickerView>(R.id.color1) }
    private val color1 = callbackFlow<Int> {
        with(picker1) {
            colorListener = ColorListener { color, _ ->
                trySendBlocking(color)
            }
            awaitClose { colorListener = null }
        }
    }.conflate()

    val picker2 by lazy { findViewById<ColorPickerView>(R.id.color2) }
    private val color2 = callbackFlow<Int> {
        with(picker2) {
            colorListener = ColorListener { color, _ ->
                trySendBlocking(color)
            }
            awaitClose { colorListener = null }
        }
    }.conflate()

    private val color = color1.combine(color2) { c1, c2 ->
        RYB.mix(Color.valueOf(c1), Color.valueOf(c2))
    }.conflate()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        picker1.setLifecycleOwner(this)
        picker2.setLifecycleOwner(this)
        val background = findViewById<View>(android.R.id.content)
        color.onEach {
            background.setBackgroundColor(it.toArgb())
        }.launchIn(lifecycleScope)
    }
}