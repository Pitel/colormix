package org.kalabovi.colormix

import android.graphics.Color
import kotlin.math.pow
import kotlin.math.sqrt

object RYB {
    // https://github.com/ProfJski/ArtColors#challenge-5-subtractive-color-mixing
    fun mix(a: Color, b: Color): Color {
        val aryb = a.toRYB()
        val bryb = b.toRYB()

        val c = Color.valueOf(1 - aryb.red(), 1 - aryb.green(), 1 - aryb.blue())
        val d = Color.valueOf(1 - bryb.red(), 1 - bryb.green(), 1 - bryb.blue())

        val f = Color.valueOf(
            (1 - c.red() - d.red()).coerceAtLeast(0f),
            (1 - c.green() - d.green()).coerceAtLeast(0f),
            (1 - c.blue() - d.blue()).coerceAtLeast(0f)
        )

        val cd = sqrt((aryb.red() - bryb.red()).pow(2) + (aryb.green() - bryb.green()).pow(2) + (aryb.blue() - bryb.blue()).pow(2)) / sqrt(3f)

        val mix1 = Color.valueOf(
            aryb.red() / 2 + bryb.red() / 2,
            aryb.green() / 2 + bryb.green() / 2,
            aryb.blue() / 2 + bryb.blue() / 2
        )

        return Color.valueOf(
            (1 - cd) * mix1.red() + cd * f.red(),
            (1 - cd) * mix1.green() + cd * f.green(),
            (1 - cd) * mix1.blue() + cd * f.blue()
        ).fromRYB()
    }

    private fun Color.toRYB(): Color {
        val CG000 = floatArrayOf(0f, 0f, 0f)
        val CG100 = floatArrayOf(.891f, 0f, 0f)
        val CG010 = floatArrayOf(0f, .714f, .374f)
        val CG001 = floatArrayOf(.07f, .08f, .893f)
        val CG011 = floatArrayOf(0f, .116f, .313f)
        val CG110 = floatArrayOf(0f, .915f, 0f)
        val CG101 = floatArrayOf(.554f, 0f, .1f)
        val CG111 = floatArrayOf(1f, 1f, 1f)

        val C00 = CG000 * (1 - red()) + CG100 * red()
        val C01 = CG001 * (1 - red()) + CG101 * red()
        val C10 = CG010 * (1 - red()) + CG110 * red()
        val C11 = CG011 * (1 - red()) + CG111 * red()

        val C0 = C00 * (1 - green()) + C10 * green()
        val C1 = C01 * (1 - green()) + C11 * green()

        val C = C0 * (1 - blue()) + C1 * blue()

        return if (C.all { it == 0f }) {
            Color.valueOf(Color.BLACK)
        } else {
            val maxsat = C * (1 / C.max())
            val clerp = lerp(C, maxsat, 0.5f)
            Color.valueOf(clerp[0], clerp[1], clerp[2])
        }
    }

    private fun Color.fromRYB(): Color {
        val CG000 = floatArrayOf(0f, 0f, 0f)
        val CG100 = floatArrayOf(1f, 0f, 0f)
        val CG010 = floatArrayOf(.9f, .9f, 0f)
        val CG001 = floatArrayOf(0f, .36f, 1f)
        val CG011 = floatArrayOf(0f, .9f, .2f)
        val CG110 = floatArrayOf(1f, .6f, 0f)
        val CG101 = floatArrayOf(.6f, 0f, 1f)
        val CG111 = floatArrayOf(1f, 1f, 1f)

        val C00 = CG000 * (1 - red()) + CG100 * red()
        val C01 = CG001 * (1 - red()) + CG101 * red()
        val C10 = CG010 * (1 - red()) + CG110 * red()
        val C11 = CG011 * (1 - red()) + CG111 * red()

        val C0 = C00 * (1 - green()) + C10 * green()
        val C1 = C01 * (1 - green()) + C11 * green()

        val C = C0 * (1 - blue()) + C1 * blue()

        return Color.valueOf(C[0], C[1], C[2])
    }

    private operator fun FloatArray.times(scale: Float) = map { it * scale }.toFloatArray()
    private operator fun FloatArray.plus(vec3: FloatArray) = mapIndexed { i, x -> x + vec3[i] }.toFloatArray()
    private fun lerp(v1: FloatArray, v2: FloatArray, amount: Float) = v1.mapIndexed { i, x -> x + amount * (v2[i] - x) }.toFloatArray()
}